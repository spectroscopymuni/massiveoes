OHAX.db
J. Luque and D.R. Crosley, “LIFBASE: Database and Spectral Simulation
Program (Version 1.5) ”, SRI International Report MP 99-009 (1999).

----------------

N2PlusBX.db
J. Luque and D.R. Crosley, “LIFBASE: Database and Spectral Simulation
Program (Version 1.5) ”, SRI International Report MP 99-009 (1999).

---------------

NHAX.db
Western C 2016 PGOPHER—a program for simulating
rotational structure, Version 9.0.100, University of Bristol

Ram R and Bernath P 2010 J. Mol. Spectrosc. 260 115–9

Lents J 1973 J. Quant. Spectrosc. Radiat. Transfer 13 297–310

Seong J, Park J K and Sun H 1994 Chem. Phys. Lett. 228 443–50

-------------------

NOBX.db
J. Luque and D.R. Crosley, “LIFBASE: Database and Spectral Simulation
Program (Version 1.5) ”, SRI International Report MP 99-009 (1999).

------------------

N2CB.db

Nassar H, Pellerin S, Musiol K, Martinie O, Pellerin N and
Cormier J 2004 J. Phys. D: Appl. Phys. 37 1904

Laux C O and Kruger C H 1992 J. Quant. Spectrosc. Radiat.
Transfer 48 9–24

Faure G and Shko’nik S 1998 J. Phys. D: Appl. Phys. 31 1212
