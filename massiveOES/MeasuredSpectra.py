# -*- coding: utf-8 -*-
"""
Created on Mon Sep 01 10:36:08 2014

@author: Petr
"""
#import image_functions
import copy
import numpy
from numpy.linalg import lstsq
import scipy.optimize.nnls
#import matplotlib.pyplot as plt
import pickle
#import simulated_spectra
from massiveOES import SpecDB, puke_spectrum, spectrum
import logging
from massiveOES.FHRSpectra import FHRSpectra
import warnings
from numpy.linalg import inv
import lmfit
import copy
import os.path
from collections import OrderedDict


class Parameters(object):
    """Class containing the parameters of the fit. Contains also instance
    of lmfit.Parameters class (as self.prms). The respective parameters can be
    accessed via __getitem__() method (i.e. brackets), just like in a
    dictionary. Apart from that contains also some extra information
    necessary for massiveOES to run properly, such as number of
    pixels and list of relevant species.

    It is usually not necessary to explicitly call any class methods, 
    as this is accomplished from MeasuredSpectra objects.
    """
    
    def __init__(self, *args, **kwargs):
        """
        wavelengths:  *iterable* with three numbers. The wavelength axis is then calculated from 
                      pixel position (pos) as 
                      lambda = wavelengths[0] + wavelengths[1]*pos + wavelengths[2]*pos**2

        slitf_gauss: gaussian HWHM of the slit function
        slitf_lorentz: lorentzian HWHM of the slit function

        baseline: *float* constant offset of the spectrum from zero

        baseline_slope: *float* accounts for non-constant baseline (necessary on some older spectrometers, 
                        otherwise should be kept fixed at 0)

        simulations: list of specDB objects, or simply empty list []
"""
        self.number_of_pixels = kwargs.pop('number_of_pixels', 1024)
        self.prms = lmfit.Parameters()
        self.info = {'species':[]}
        wavs = kwargs.pop('wavelengths', (0,1,0))
        self.prms.add('wav_start', value=wavs[0])
        self.prms.add('wav_step', value=wavs[1])
        self.prms.add('wav_2nd', value=wavs[2])
        
        gauss = kwargs.pop('slitf_gauss', 1e-9)
        lorentz = kwargs.pop('slitf_lorentz', 1e-9)
        self.prms.add('slitf_gauss', value=gauss)
        self.prms.add('slitf_lorentz', value=lorentz)
    
        baseline = kwargs.pop('baseline', 0)
        self.prms.add('baseline', value=baseline)

        baseline_slope = kwargs.pop('baseline_slope', 0)
        self.prms.add('baseline_slope', value=baseline_slope)

        simulations = kwargs.pop('simulations', None)
        if simulations is not None:
            for sim in simulations:
                self.add_specie(sim)

    def __getitem__(self, key):
        return self.prms.__getitem__(key)

    def keys(self):
        return self.prms.keys()
        
    def add_specie(self, specie, **kwargs):
        """
        specie: specDB object
        """
        Trot = kwargs.pop('Trot', 1e3)
        Tvib = kwargs.pop('Tvib', 1e3)
        intensity = kwargs.pop('intensity', 1)
        specie_name = specie.specie_name
        if lmfit.astutils.valid_symbol_name(specie_name) and specie_name not in self.info['species']:
            self.info['species'].append(specie_name)
            #self.info[specie_name+'_sim'] = specie # specDB object
            self.prms.add(specie_name+'_Trot', value=Trot)
            self.prms[specie_name+'_Trot'].min = 300
            self.prms[specie_name+'_Trot'].max = 10000
            self.prms.add(specie_name+'_Tvib', value=Tvib)
            self.prms[specie_name+'_Tvib'].min = 300
            self.prms[specie_name+'_Tvib'].max = 10000
            self.prms.add(specie_name+'_intensity', value=intensity)
            self.prms[specie_name+'_intensity'].min = 0
        else:
            msg = 'Specie \''+ specie_name +'\' not added: Invalid name or already added! See lmfit.Parameters for details.'
            warnings.warn(msg,Warning)

    def rm_species(self, specie):
        """
        Remove simulated spectrum of given specie.

        specie: string, name of the specie (eg. \'OH\')
        """
        self.info['species'].remove(specie)
        #self.info.pop(specie+'_sim', 0)
        #print specie
        self.prms.pop(specie+'_Trot', 0)
        self.prms.pop(specie+'_Tvib', 0) 
        self.prms.pop(specie+'_intensity',0)
        

    def save(self, filename):
        with open(filename, 'wb') as output:
            pickle.dump(self, output, pickle.HIGHEST_PROTOCOL)


    @staticmethod
    def load(filename):
        with open(filename, 'rb') as input:
            return_val = pickle.load(input)
        return return_val    


class MeasuredSpectra():
    """Class containing the measured data. Suitable for storing number of
    spectra (high numbers are possible, but it is not optimized to be
    memory efficient - all spectra will be loaded into the memory at
    initiating an instance of MeasuredSpectra. A reasonable strategy
    is to divide large measurements into several MeasuredSpectra
    objects.

    It also keeps references to spectral simulations and contains
    methods for least squares fitting.

    """


    def __init__(self, **kwargs):
        """Most of the kwargs are never used in the class methods and serve
        only for future reference - to allow the experimenter to keep
        track of the important metadata.

        The only exception is \'spectra'\ that can be used to fill in
        the measured data. The format of this should be a list of
        iterables (tuples or lists) containing [identificator, data],
        where identificator can be a string or a number (can contain
        e.g. spatial position or delay after the trigger) and data
        should be 1D numpy array (a vector) of \'intensity\'
        values. The wavelengths are not contained here, but are always
        calculated from Parameters (see class Parameters).

        kwargs:
        
        spectra: a list of tuples or lists, each containing ID (arbitrary, serves to the experimenter) and 
                 a 1D numpy array of y-values 

        filename: *string* - name of the file with the source data. Only for future reference, can be omitted.

        filenameSPE: *string* obsolete, used to keep track of the original SPE files from PIMAX ICCD camera

        date: defaults to 0, but can be used to keep the date of the measurement. Format is arbitrary.

        time: defaults to 0, but can be used to keep the time of the measurement. Format is arbitrary.

        accumulations: to keep track of the number of accumulations.

        """

        self.filename = kwargs.pop('filename', '')
        self.filenameSPE = kwargs.pop('filenameSPE','')
        self.date = kwargs.pop('date',0)
        self.time = kwargs.pop('time',0)
        self.accumulations = kwargs.pop('accumulations',0)
        self.gatewidth = kwargs.pop('gatewidth',0)
        self.regionofinterest_y = kwargs.pop('ROI_y',None)
        self.regionofinterest_x = kwargs.pop('ROI_x',None)
        
        #self.fiberposition = 0
        #self.fibersteepnes = 0
        #self.fiberverticalstep = 0
        
        self.spectra = kwargs.pop('spectra',[])
        self.image_analysis_values = []
        self.image_analysis_values_used = []
        self.parameters_list = []       
        self.minimizer=None
        self.minimizer_result = None
        self.simulations = {}

    def __getstate__(self):
        ret = (self.filename, self.filenameSPE, self.date, self.time, self.accumulations, 
               self.gatewidth, self.regionofinterest_x, self.regionofinterest_y,
               self.spectra, self.parameters_list, self.simulations)
        return ret

    def __setstate__(self, state):
        self.filename, self.filenameSPE, self.date, self.time, self.accumulations, self.gatewidth, self.regionofinterest_x, self.regionofinterest_y,  self.spectra, self.parameters_list,self.simulations = state


    @classmethod
    def from_FHRSpectra(cls, FHRSpec):
        """
        for internal use only
        """
        #print( FHRSpec.wavs)
        #print( FHRSpec.intensities)
        #print( FHRSpec.delays)
        spectra = []
        wavs = [numpy.min(FHRSpec.wavs), numpy.mean(numpy.diff(FHRSpec.wavs)), 0]
        for i in range(len(FHRSpec.delays)):
            if wavs[1] < 0:
                #reverse order of intensities if wavelength ordered descendingly
                spectra.append((FHRSpec.delays[i], FHRSpec.intensities[i][::-1]))
            else:
                spectra.append((FHRSpec.delays[i], FHRSpec.intensities[i]))
        if wavs[1] < 0:
            wavs[1] *= -1
                    
        ret = cls(spectra=spectra)

        return ret, wavs
    
    @classmethod    
    def from_CSV(cls, filename):
        """Used to extract measured data from ASCII files containing the
        wavelengths in the first column and and intensity vectors of
        y-values in other columns. Delimiter must be coma:

        w1,y11,y21
        w2,y12,y22
        w3,y12,y23
        ...

        """
    # loads data from csv, expects first column of wavelengts and coma as separator
        dataarray = numpy.genfromtxt(filename, delimiter = ',')
        wavs = [dataarray[0,0], dataarray[1,0] - dataarray[0,0],0]
        spec = []
        for i in range(1, dataarray.shape[1]):
            spec.append((i, dataarray[:,i]))
            
        ret = MeasuredSpectra(filename = filename, spectra = spec)

        ret.create_fit_parameters([],wavs)
        return ret

    @staticmethod
    def from_FHRfile(filename, simulated_spectra=[]):
        """used to extract measured data from ASCII files containing the
        wavelengths in the first row and ID and intensity vector in
        each other row, i.e.:
            \t w1  \t w2  \t w3...
        ID1 \t y11 \t y12 \t y13...
        ID2 \t y21 \t y22 \t y23...
        ...

        """
        fhrs = FHRSpectra.from_file(filename = filename)
        ret, wavs = MeasuredSpectra.from_FHRSpectra(fhrs)
        ret.create_fit_parameters(simulated_spectra, wavs)
        return ret

    def add_specie(self, specie, index):
        """use this spectral simulation for comparison with measured data at
        given index. The reference to simulation object will be added
        to self.simulations and a string describing the specie will be
        added to Parameters.info dictionary.
        
args:
   specie: specDB object
   index: to which Parameters object the specie should be added

        """
        #specie_name = specie.specie_name
        if specie.specie_name not in self.simulations:
            self.simulations[specie.specie_name] = specie
        self.parameters_list[index].add_specie(specie)
 
     
        
    # def spectra_from_spe(self, filename):
    #     from piUtils import readSpe
    #     input_whole = readSpe(filename)        
        
    #     self.filename = filename
    #     self.filenameSPE = input_whole['SPEFNAME']
    #     self.date = input_whole['OBSDATE']
    #     self.accumulations = input_whole['OnCcdAcc']
    #     self.gatewidth = input_whole['GATEWIDTHus']
    #     self.regionofinterest_x = input_whole['ROI_X']
    #     self.regionofinterest_y = input_whole['ROI_Y']
        
    #     image_data = numpy.array(readSpe(filename)['data'])
        
    #     self.spectra,self.image_analysis_values_used, self.image_analysis_values = image_functions.image_to_spectras(image_data)

        
    def create_fit_parameters(self, simulated_spectra, wavelengths, **kwargs):      
        """Args:
    simulated_spectra: *list* of SpecDB objects

    Wavelengths: tuple/list with meaning [0]: wavelength_start, [1]:
    wavelength_step (difference of wavelength position of two
    neiboughring pixels), [2]: 2nd order correction. The resulting
    wavelegth axis is computed from pixel index 'pos' as x = wav[0] +
    wav[1]*pos + wav[2]*pos**2

**kwargs: 
        passed directly to Parameters.__init__()

        """
        self.parameters_list = []
        for spec in self.spectra:
            self.parameters_list.append(Parameters(simulations=simulated_spectra, 
                                                   wavelengths=wavelengths, 
                                                   number_of_pixels=len(spec[1]), **kwargs))
        
    # def subtract_background(self, **kwargs):
    #     from piUtils import readSpe
    #     background_filename = kwargs.pop('filename', '20p7_fail.SPE')
    #     background_image_range = kwargs.pop('imagerange', [111,190])        
        
    #     background = readSpe(background_filename)
    #     background_gatewidth = background['GATEWIDTHus']
    #     background_onCCDAcc = background['onCCDAcc']
    #     background_image = numpy.array(background['data'])
    #     background_image_selectedrange =  background_image[background_image_range[0]:background_image_range[1],:,:]
        
    #     background_spectra_list =[]
    #     background_rotated = image_functions.offset_removal(background_image_selectedrange, steepnes = self.image_analysis_values_used[0]*0.01)
    #     background_rotated_mean = numpy.mean(background_rotated[:,:,:], axis = 0)        
        
    #     dim = background_rotated_mean.shape
    #     dim_y = int(dim[0])
    #     steps_y = int(dim_y/self.image_analysis_values_used[2])
        
    #     for i in range(0,steps_y):
    #         if (((int(self.image_analysis_values_used[1] + i*self.image_analysis_values_used[2]) -3) > 0) and ((int(self.image_analysis_values_used[1] + i*self.image_analysis_values_used[2]) +3) < dim_y)):
    #             current_spectra = numpy.mean(background_rotated_mean[int(self.image_analysis_values_used[1] + i*self.image_analysis_values_used[2])-3:int(self.image_analysis_values_used[1] + i*self.image_analysis_values_used[2])+3,:], axis = 0)
    #             background_spectra_list.append([int(self.image_analysis_values_used[1] + i*self.image_analysis_values_used[2]), current_spectra])
    #     background_multiplier = background_gatewidth*background_onCCDAcc/(self.accumulations*self.gatewidth)
        
    #     for i in range(0,len(self.spectra)):
    #         self.spectra[i][1] = self.spectra[i][1] - background_multiplier*background_spectra_list[i][1]
        
        
    def plot(self, **kwargs):
        """use matplotlib to visualise measured data. The x-axis is not
        calculated, the pixel number is used instead. 

        kwargs: 

        plotrange: [from, to] (defaults to [0, len(self.spectra)]. 
                   Which spectra will be show in the plot.

        """
        import matplotlib.pyplot as plt
        plot_range = kwargs.pop('plotrange', [0,len(self.spectra)])
        for i in range(plot_range[0],plot_range[1]):   
            plt.plot(self.spectra[i][1])
        plt.show()       
        
    def save(self, filename):
        self.minimizer=None
        with open(filename, 'wb') as output:
            pickle.dump(self, output, pickle.HIGHEST_PROTOCOL)
            

    
    def prepare_for_nnls(self, index, species, **kwargs):
        #import matplotlib.pyplot as plt
        prms = self.parameters_list[index]
        wav = kwargs.pop('wavelength', 'air') 
        kwargs['wavelength'] = wav
        wav += '_wavelength'
        numpoints = len(self.spectra[index][1])
        start = prms['wav_start'].value
        end = start + prms['wav_step'].value*numpoints + prms['wav_2nd'].value * numpoints**2
        info = OrderedDict()
        A = []
        to_match = self.measured_Spectra_for_fit(index)
        for specie in species:
            if specie in prms.info['species']:
                if specie in kwargs:
                    print (specie, kwargs[specie])
                    info[specie] = self.simulations[specie].get_lines_by_states(start, end, **kwargs[specie])
                else:
                    print(kwargs)
                    info[specie] = self.simulations[specie].get_lines_by_states(start, end, **kwargs)              

                for spec in info[specie]['specs']:
                    spec = numpy.vstack(([min(to_match.x), 0], 
                                         spec, 
                                         [max(to_match.x), 0]))
                    spec = spectrum.Spectrum(x = spec[:,0], y = spec[:,1])
                    spec.refine_mesh()
                    spec.convolve_with_slit_function(gauss=prms['slitf_gauss'].value,
                                                   lorentz=prms['slitf_lorentz'].value,
                                                     step=prms['wav_step'].value)
                    spec = spectrum.match_spectra(spec, to_match)[0]
                    A.append(spec.y)
            info[specie].pop('specs')

        return numpy.array(A).T, info
                


    def fit_nnls(self, index, species, baseline=False, baseline_slope=False, **kwargs):
        """State-by-state fit of molecular simulation to measured data in
        order to construct Boltzmann plot.
        
        args:
        -----
        index: *int* which of the measured spectra is of interest

        species: *list of strings* identifying the desired
                 species. The species need to be added to MeasuredSpectra
                 object before calling this method (not necesarilly to the parameters at this index).

        baseline: *bool* if True, the constant baseline will be fitted
                   as well. If False, the baseline from
                   self.parameteres_list[index]['baseline'] will be subtracted
                   from the measured data before performing the fit

        baseline_slope: *bool* if True, the tilted background will be fitted
                        as well. If False, the tilted background from
                        self.parameteres_list[index]['baseline_slope'] will be subtracted
                        from the measured data before performing the fit

        kwargs:
        -------
        A dictionary of additional parameters for each fitted specie. 
        These are:

        max_v: *number* the maximal vibrational quantum number allowed
        
        max_J: *number* the maximal rotational quantum number allowed

        minlines: *number* the minimal number of lines emitted by transition 
                   from given upper state in the observed wavelength region. 
                   This is recommended to be at least 2 to suppress the 
                   influence of noise. 

        singlet_like: *bool* if True, the spin-orbit or spin-rotational 
                      components of the upper states will not be distinguished

        """
        specs, info = self.prepare_for_nnls(index, species, **kwargs)
        if baseline:
            info['baseline'] = True
            tostack = numpy.ones((specs.shape[0],1))
            specs = numpy.hstack((specs, tostack))
            if hasattr(self.spectra[index][1], 'y'):
                tofit = self.spectra[index][1].y
            else:
                tofit = self.spectra[index][1]
        else:
            info['baseline'] = False
            if hasattr(self.spectra[index][1], 'y'):
                tofit = self.spectra[index][1].y - self.parameters_list[index]['baseline'].value
            else:
                tofit = self.spectra[index][1] - self.parameters_list[index]['baseline'].value
            

        if baseline_slope:
            #tostack = numpy.arange(specs.shape[0])
            params = self.parameters_list[index]
            tostack = self.get_wavs(specs.shape[0], 
                                    params['wav_start'].value, 
                                    params['wav_step'].value,
                                    params['wav_2nd'].value)
            specs = numpy.hstack((specs, tostack.reshape((specs.shape[0], 1))))
            info['baseline_slope'] = True
        else:
            #print(self.parameters_list[index]['baseline_slope']*
            #        numpy.arange(len(tofit)))
            info['baseline_slope'] = False
            tofit -= self.parameters_list[index]['baseline_slope']*numpy.arange(len(tofit))

        pops, rnorm = scipy.optimize.nnls(specs,tofit)
        errs = MeasuredSpectra.nnls_errors(specs, rnorm, pops)
        frame = MeasuredSpectra.reorganize_nnls_to_pandas(pops, info, errs)  
        return frame, specs
        #return pops, info, rnorm


    @staticmethod
    def reorganize_nnls_to_pandas(pops, info, errs):
        """
        internal method for creating convenient pandas dataframes 
        from original output of MeasuredSpectra.fit_nnls()
        """
        import pandas
        frames = []
        slice_start = 0
        for specie in info:
            if specie.startswith('baseline'):
                continue
            #print 'reorganize: len of state = ',  len(info[specie]['states'][0])

            #v = [state[0] for state in info[specie]['states']]
            #J = [state[1] for state in info[specie]['states']]
            #if len(info[specie]['states'][0]) == 3:
            #    component = [state[2] for state in info[specie]['states']]
            #else:
            #    component = [numpy.nan for state in info[specie]['states']]
            df = info[specie]['states']
            slice_stop = slice_start+len(df)
            df['pops'] = pops[slice_start:slice_stop]
            df['errors'] = errs[slice_start:slice_stop]
            df['specie'] = specie
            frames.append(df)
            slice_start = slice_stop
        ret = pandas.concat(frames)
        ret.reset_index(drop=True, inplace = True)
        return ret

    @staticmethod
    def nnls_errors(jac, rnorm, pops):
        """
        An internal function for calculating errors of the fit_nnls result. 

        args:
        jac: Jacobian matrix

        rnorm: sum of squared residuals

        pops: results of the fit
        """
        print(jac.shape)
        hess = numpy.dot(jac.T, jac)
        dof = jac.shape[0] - len(pops)
        print('dof = ', dof)
        covmat = rnorm/(dof)*numpy.linalg.inv(hess)
        return abs(numpy.diag(covmat))**0.5

    # def Boltzmann_plot(self, index, species, **kwargs):
    #     pops, info, rnorm = self.fit_nnls(index, species, **kwargs)
    #     i = 0
    #     res = []
    #     for specie in info:
    #         if specie.startswith('baseline'):
    #             continue
    #         for E_J, E_v, state in zip(info[specie]['E_J'], info[specie]['E_v'], info[specie]['states']):
    #             res.append([E_J, E_v, numpy.log(pops[i]/(2*state[1] + 1))])
    #             i+=1
    #     return numpy.array(res)
                



    def measured_Spectra_for_fit(self, index, params = None):
        """
        Return the measured spectrum at given index, including the wavelength axis.

        args:
        index: *int* which spectrum to return

        params: *instance of massiveOES.Parameters*, defaults to None, 
                in which case the self.parameters_list[index] is used

        return:
        
        an instance of massiveOES.Spectrum() with attributes x and y 
        """
        
        meas_spectra_y = self.spectra[index][1] #+ self.parameters_list[index].parameters_dict[('Baseline')]

        #the MeasuredSpectra objects can newly contain spectra including
        #the x-axis. In that case, the wav_... params are ignored
        #used for cases when the existing wavelength calibration is more correct
        #than the 2nd order polynomial approximation (typically spectra glued from 
        #more windows and carefully pre-processed to fit
        if hasattr(meas_spectra_y, 'x') and hasattr(meas_spectra_y, 'x'):
            return meas_spectra_y

        if params is None:
            params = self.parameters_list[index]

        wavs = MeasuredSpectra.get_wavs(len(self.spectra[index][1]),
                                        params['wav_start'].value,
                                        params['wav_step'].value,
                                        params['wav_2nd'].value)

        meas_spectra =  spectrum.Spectrum(y=meas_spectra_y , 
                                          x = wavs)           
        logging.debug('meas_spectra: ')
        logging.debug('x = [%f ... %f]', meas_spectra.x[0], meas_spectra.x[-1])
        logging.debug('y = [%f ... %f]', meas_spectra.y[0], meas_spectra.y[-1])
        return meas_spectra
        
    @staticmethod
    def get_wavs(numpoints, start, step, second):
        pos = numpy.arange(numpoints)
        ret = start + pos*step + pos**2*second
        return ret
        
    
    def show(self, index, save_to_file = None, **kwargs):
        """
Show the agreement of the measured data with the simulation with current parameters.
Args:
        index: (*int*) index of the current measured spectrum
        save_to_file: *string* containing the filename that the plot will be saved to. If None (default), the plot will be shown.

**kwargs:
        convolve: *bool*, Goes directly to simulated_spectra.puke_spectrum()
        """
        import matplotlib.pyplot as plt
        convolve = kwargs.pop('convolve', True)
        mtpl = self.measured_Spectra_for_fit(index)
        simtpl =  puke_spectrum(self.parameters_list[index], convolve = convolve, sims=self.simulations)
        plt.plot(mtpl.x, mtpl.y, label = 'Measured')
        plt.plot(simtpl.x, simtpl.y, label = 'Fitted')
        plt.xlim(min(mtpl.x), max(mtpl.x))
        plt.legend()
        plt.xlabel('wavelength [nm]')
        plt.ylabel('intensity [arb. units]')
        if save_to_file is None:
            plt.show()
        else:
            plt.savefig(save_to_file)
        return

    # def check_simulations(self):
    #     species = self.get_all_species()
    #     spec_paths = []
    #     for params in self.parameters_list:
    #         for spec in params.info['species']:
    #             spec_paths.append(params.info[spec+'_sim'].status['sim_filename'])
    #     spec_paths = set(spec_paths)
    #     invalid_paths = []
    #     for path in spec_paths:
    #         if not os.path.isfile(path):
    #             invalid_paths.append(path)
    #     return invalid_paths

        
    # def replace_simulations(self, list_of_sims):
    #     invalid_paths = self.check_simulations()
    #     sims = {}
    #     for sim in list_of_sims:
    #         sims[sim.status['species']] = sim
    #     for param in self.parameters_list:
    #         for specie in param.info['species']:
    #             if (param.info[specie+'_sim'].status['sim_filename'] 
    #                 in invalid_paths) and (specie in sims):
    #                 param.info[specie+'_sim'] = sims[specie]
    #                 #param.rm_species(specie)
    #                 #param.add_specie(sims[specie])


    def to_json(self, filename):
        import json

        spectra = OrderedDict()
        params = OrderedDict()
        for pair, par in zip(self.spectra, self.parameters_list):
            if hasattr(pair[1], 'x'):
                spectra[str(pair[0])] = {'x':list(pair[1].x),
                                    'y':list(pair[1].y)}
            else:
                spectra[str(pair[0])] = list(pair[1])

            params[str(pair[0])] = {'number_of_pixels':par.number_of_pixels,
                    'info':par.info,
                    'prms':par.prms.dumps()}

        simulations = list(self.simulations.keys())
        to_save = {'spectra':spectra,
                'params':params,
                'simulations':simulations
                }

        with open(filename,'w') as fp:
            json.dump(to_save, fp)

    @staticmethod
    def from_json(filename):
        import json
        with open(filename, 'r') as fp:
            loaded = json.load(fp, object_pairs_hook=OrderedDict)
        spectra = loaded['spectra']
        spec = []
        for s in spectra:
            if hasattr(spectra[s], 'keys'):
                print('found Spectrum!')
                spec.append([s, 
                    spectrum.Spectrum(x = numpy.array(spectra[s]['x']),
                               y = numpy.array(spectra[s]['y']))])
            else:
                spec.append([s, numpy.array(spectra[s])])
        
        sims = {}
        for sim in loaded['simulations']:
            sims[sim] = SpecDB(sim + '.db') 

        parameters_list = []
        for param in loaded['params']:
            to_app = Parameters(number_of_pixels=loaded['params'][param]['number_of_pixels'])
            to_app.info = loaded['params'][param]['info']
            try:
                to_app.prms.loads(loaded['params'][param]['prms'])
            except TypeError:
                for entry in json.loads(loaded['params'][param]['prms']):
                    to_app.prms.add(entry[0], value=entry[1], 
                            vary=entry[2], min=entry[4], max=entry[5])
            parameters_list.append(to_app)

        ret = MeasuredSpectra(spectra = spec)

        ret.simulations = sims
        ret.parameters_list = parameters_list
        return ret


    @staticmethod
    def load(filename):
        """
        load previously saved MassiveOES object
        """
        print(":::::::::::::::::::::::::::::::::::")
        print(filename)
        with open(filename, 'rb') as input:
            loaded = pickle.load(input)
        # invalid_paths = return_val.check_simulations()
        # for ip in invalid_paths:
        #     msg = 'The simulation '+ ip + ' was not found!'
        #     warnings.warn(msg, Warning)
        ret = MeasuredSpectra()
        ret.filename =  loaded.filename  
        ret.filenameSPE = loaded.filenameSPE 
        ret.date = loaded.date
        ret.time = loaded.time
        ret.accumulations = loaded.accumulations
        ret.gatewidth = loaded.gatewidth
        ret.regionofinterest_x = loaded.regionofinterest_x
        ret.regionofinterest_y = loaded.regionofinterest_y
        ret.spectra =  loaded.spectra
        ret.parameters_list = loaded.parameters_list
        ret.simulations = loaded.simulations
        return ret

    
    def _dummy(self, params, index, **kwargs):
        """
        method with the desired signature for lmfit.minimize
        """
        #print '*********index = ', index, ' ************'
        #reduced_sumsq = kwargs.pop('reduced_sumsq', False)
        by_peaks = kwargs.pop('by_peaks', False)
        convolve = kwargs.pop('convolve', True)
        weighted = kwargs.pop('weighted', False)
        #step = numpy.abs(numpy.mean(numpy.diff(self.measured_Spectra_for_fit(index).x)))        
        step = params['wav_step'].value

        #temp_params = copy.deepcopy(self.parameters_list[index])
        #temp_params.prms = params
        self.parameters_list[index].prms = params
        params = self.parameters_list[index]
        
        #print temp_params['wav_start'].vary
        #print temp_params['wav_step'].vary

        #self.parameters_list[index] = params
        #print params.prms
        #print params['OH_Trot']
        if by_peaks: #and reduced_sumsq:
            return spectrum.compare_spectra_by_peaks_reduced_sumsq\
                   (self.measured_Spectra_for_fit(index, params=params),
                    puke_spectrum(temp_params,
                                  convolve=convolve, step = step, sims = self.simulations), **kwargs)
        # if by_peaks and not reduced_sumsq:
        #     return spectrum.compare_spectra_by_peaks\
        #            (self.measured_Spectra_for_fit(index),
        #             simulated_spectra.puke_spectrum(self.parameters_list[index],
        #                                             convolve=convolve, step = step), **kwargs)
        # if not by_peaks and reduced_sumsq:
        #     return spectrum.compare_spectra_reduced_susmq\
        #            (self.measured_Spectra_for_fit(index),
        #             simulated_spectra.puke_spectrum(self.parameters_list[index], 
        #                                             convolve=convolve, step = step), **kwargs)
        if not by_peaks and not weighted:
            return spectrum.compare_spectra\
                   (self.measured_Spectra_for_fit(index, params = params),
                    puke_spectrum(params,
                                  convolve=convolve, step = step, sims = self.simulations), **kwargs)
        if not by_peaks and weighted:
            return spectrum.compare_spectra_weighted\
                   (self.measured_Spectra_for_fit(index, params = params),
                    puke_spectrum(temp_params,
                                  convolve=convolve, step = step, sims = self.simulations), **kwargs)



    def fit(self, index, **kwargs):
        """Find optimal values of the fit parameters for spectrum at given
        index. The optimal values are then stored in
        self.parameters_list[index], not returned!

        args: 
        -----
        index: *int* which spectrum to fit

        **kwargs:
        ---------
        maxiter: *int* maximal number of itertions, handy with slower optimization methods

        method: *string* see lmfit documentation for available methods

        by_peaks: *bool* defaults to False. If you own echelle spectrometer, 
                  play around with enabling this option. Otherwise, leave it at false. 
                  Never properly tested.

        return:
        -------
        result: *bool*, True if the fit converged successfully, False otherwise

        """
        #by_peaks = kwargs.pop('by_peaks', False)

        #p = self.parameters_list[index]

        # if p['wav_start'].vary or p['wav_step'].vary or p['wav_2nd'].vary:
        #     kwargs['sumsq'] = True
        # else:
        #     kwargs['sumsq'] = False
        #sumsq = kwargs.pop('sumsq', sumsq)
        #if self.minimizer is None:
        print('********* index = ', index, ' ************')
        kwargs['number_of_pixels'] = self.parameters_list[index].number_of_pixels
        maxiter = kwargs.pop('maxiter', 2000)
        #options = {'maxiter':maxiter}
        method = kwargs.pop('method', 'leastsq')
        if method == 'leastsq':
            self.minimizer = lmfit.Minimizer(self._dummy, 
                                             self.parameters_list[index].prms, 
                                             fcn_args=(index,),
                                         # fcn_kws={'by_peaks':by_peaks, 
                                         #          'reduced_sumsq':sumsq, 
                                         #          'convolve':convolve}
                                             fcn_kws = kwargs,
                                             maxfev=maxiter)
        else:
            self.minimizer = lmfit.Minimizer(self._dummy, 
                                             self.parameters_list[index].prms, 
                                             fcn_args=(index,),
                                         # fcn_kws={'by_peaks':by_peaks, 
                                         #          'reduced_sumsq':sumsq, 
                                         #          'convolve':convolve}
                                             fcn_kws = kwargs,
                                             options = {'maxiter':maxiter, 'xtol':0.05})
            
        # if kwargs['by_peaks']:
        #     method = kwargs.pop('method', 'L-BFGS-B')
        #     self.minimizer.scalar_minimize(method=method)
        # if not kwargs['by_peaks']:
        

        #print "FITUJU METODOU:   ", method
        self.minimizer_result = self.minimizer.minimize(method=method)
        self.parameters_list[index].prms = self.minimizer_result.params
        return self.minimizer_result
        
    def get_all_species(self):
        """
        internal method to determine which species are used throughout the simulation
        """
        all_specs = []
        for p in self.parameters_list:
            all_specs.extend(p.info['species'])
        return set(all_specs)

    def export_results(self, filename):
        """
        Save the results of the optimisation as csv file. Uses pandas. 

        args:
        -----
        filename: *string* a name of the file, the data will be exported to. 
                  Does NOT ask for confirmation before overwritng!
        """
        
        import pandas
        all_species = list(self.get_all_species())

        #result = numpy.zeros((len(self.spectra), 2+6*len(all_species)))
        result = {'spectrum':[],
                  'reduced_sumsq':[]}
        #result[:,:] = numpy.nan

        for specie in all_species:
            result[specie+'_Trot'] = []
            result[specie+'_Trot_dev'] = []
            result[specie+'_Tvib'] = []
            result[specie+'_Tvib_dev'] = []
            result[specie+'_intensity'] = []
            result[specie+'_intensity_dev'] = []
        
        for row in range(len(self.spectra)):
            #result[row, 0] = self.spectra[row][0]
            result['spectrum'].append(self.spectra[row][0])
            # if the list of simulations is empty, do not calculate residuals
            if not self.parameters_list[row].info['species']:
                sumsq = numpy.nan
            else:
                residuals = self._dummy(self.parameters_list[row].prms, row)
                #residuals /= numpy.sum(self.spectra[row][1])
                sumsq = numpy.sum(residuals[~numpy.isinf(residuals)]**2)
                sumsq /= numpy.sum(self.spectra[row][1] - self.parameters_list[row]['baseline'].value - 
                        self.parameters_list[row]['baseline_slope']*numpy.arange(len(self.spectra[row][1])))
            #result[row, 1] = sumsq
            result['reduced_sumsq'].append(sumsq)
            for specie in all_species:
                if specie in self.parameters_list[row].info['species']:
                    result[specie+'_Trot'].append(
                        self.parameters_list[row][specie + '_Trot'].value)

                    result[specie+'_Trot_dev'].append(
                        self.parameters_list[row][specie + '_Trot'].stderr)

                    result[specie+'_Tvib'].append(
                        self.parameters_list[row][specie + '_Tvib'].value)

                    result[specie+'_Tvib_dev'].append(
                        self.parameters_list[row][specie + '_Tvib'].stderr)

                    result[specie+'_intensity'].append(
                        self.parameters_list[row][specie + '_intensity'].value)

                    result[specie+'_intensity_dev'].append(
                        self.parameters_list[row][specie + '_intensity'].stderr)
                else:
                    result[specie+'_Trot'].append(numpy.nan) 
                    result[specie+'_Tvib'].append(numpy.nan) 
                    result[specie+'_Trot_dev'].append(numpy.nan) 
                    result[specie+'_Tvib_dev'].append(numpy.nan) 
                    result[specie+'_intensity'].append(numpy.nan)
                    result[specie+'_intensity_dev'].append(numpy.nan)

        out = pandas.DataFrame(result)
        out.to_csv(filename)
        return out

    # def _fix_params(self, index, params_given):
    #     params_inner = self.parameters_list[index]
    #     for param in ['wav_start',
    #                   'wav_step',
    #                   'wav_2nd',
    #                   'baseline',
    #                   'baseline_slope',
    #                   'slitf_gauss',
    #                   'slitf_lorentz']:
    #         for suff in ['min', 'max', 'vary', 'value']:
    #             exec('params_inner[\'' + param + '\'].' +suff + '=params_given[\''+param+'\'].'+suff)

    #     for specie in params_given.info['species']:
            

