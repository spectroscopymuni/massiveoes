import sqlite3 as sqlite
import pandas
from massiveOES import spectrum
import os
import warnings
import numpy
from scipy.constants import physical_constants, h, c
from copy import copy

path = os.path.dirname(spectrum.__file__)

##############
##for pyinstaller
#import sys
#path = sys._MEIPASS
#####################


kB = physical_constants['Boltzmann constant in inverse meters per kelvin'][0]
kB /=100 #inverse cm


class SpecDB(object):
    """ 
    Class for working with spectral databases, using pandas
    """
    def __init__(self, filename):
        """
args: 
-----
filename: name of the database file. Just the file, not the path. The files
will be ALWAYS searched for exclusively in massiveOES_location/data
directory. Providing full path will result in error. 
        """
        name_stop = filename.find('.')
        self.specie_name = copy(filename[:name_stop])
        self.filename = filename
        to_open = os.path.join(path,'data',filename)
        if not SpecDB.isSQLite3(to_open):
            warnings.warn('The file '+to_open+
                        ' is not a valid database!')
            self.conn = None
            return
        else:
            self.conn = sqlite.connect(to_open)
        self.upper_states = pandas.read_sql_query("select J,E_J,E_v from upper_states", 
                self.conn)
        self.last_Trot = None
        self.last_Tvib = None
        self.norm = None
        self.spec = None
        self.last_wmin = 0 
        self.last_wmax = numpy.inf
        self.table = None




    @staticmethod
    def isSQLite3(filename):
        from os.path import isfile, getsize
        if not isfile(filename):
            return False
        if getsize(filename) < 100: # SQLite database file header is 100 bytes
            return False

        with open(filename, 'rb') as fd:
            header = fd.read(100)
        return header[:16] == b'SQLite format 3\x00'


    def __getstate__(self):
        return self.filename

    def __setstate__(self, state):
        filename = state
        ret = self.__init__(filename)
        return ret

    def calculate_norm(self, Trot, Tvib):
        parts = (2*self.upper_states.J+1)*numpy.exp(-self.upper_states.E_J /
                (kB*Trot) - self.upper_states.E_v / (kB*Tvib))
        return numpy.sum(parts)


    def get_spectrum(self, Trot, Tvib, wmin=None, wmax = None, **kwargs):
        """
kwargs:
   wavelength: either 'vacuum' or 'air', returns the wavelength in vacuum or air (default is 'air')
    
   as_spectrum: bool, if True object massiveOES.Spectrum is returned, otherwise a numpy array is returned

    y: either 'photon_flux' (default), i.e. the y*axis is population * (emission coefficient)
      or 'intensity', i.e. the y-axis is population * (emission coefficient) * wavenumber
        """
        as_spectrum = kwargs.pop('as_spectrum', True)
        wav_reserve = kwargs.pop('wav_reserve', 2)
        # print '****************************'
        # print Trot, self.last_Trot
        # print Tvib, self.last_Tvib
        # print wmin, self.last_wmin
        # print wmax, self.last_wmax
        
        y = kwargs.pop('y', 'photon_flux')
        wav = kwargs.pop('wavelength', 'air')
        wav += '_wavelength' 
        recalculate_pops = False

        if (wmin < self.last_wmin or wmax > self.last_wmax or 
                self.table is None):
            self.last_wmin = wmin - wav_reserve
            self.last_wmax = wmax + wav_reserve
            self.table = self.get_table_from_DB(self.last_wmin, 
                    self.last_wmax, wav=wav,**kwargs)
            recalculate_pops = True

        if (self.last_Trot != Trot or self.last_Tvib != Tvib or 
                recalculate_pops):  
            self.norm = self.calculate_norm(Trot, Tvib)
            self.last_Trot = Trot
            self.last_Tvib = Tvib
            self.table['pops'] = (2*self.table['J']+1)*numpy.exp(-self.table['E_v']/(kB*Tvib) - 
                    self.table['E_J']/(kB*Trot)) / self.norm

            #print('norm =', self.norm)
        self.table['y'] = self.table['pops'] * self.table['A']
        if y == 'intensity':
            self.table['y'] *= self.table['wavenumber'] 
        if as_spectrum:
            self.spec = spectrum.Spectrum(x = self.table[wav], 
                    y = self.table['y'])
        else:
            self.spec = numpy.array([self.table[wav], self.table['y']]).T
        return copy(self.spec)


    def get_table_from_DB(self, wmin=None, wmax=None, **kwargs):
        """
        """
        wav = kwargs.pop('wav', 'air_wavelength')
        q = 'SELECT air_wavelength,vacuum_wavelength,A,J,E_J,E_v,wavenumber'
        q+= ' FROM '
        if wmin != 0  and wmax != numpy.inf:
            q += " (SELECT * FROM lines WHERE " + wav + " BETWEEN  "
            q += str(wmin) + " AND " + str(wmax) + ")"
        else:
            q += " lines "
        q += " INNER JOIN upper_states on upper_state=upper_states.id  ORDER BY " + wav
        #print('******pandas************')
        #print(q)
        table = pandas.read_sql_query(q,self.conn)
        return table


    def get_lines_by_states(self, wmin, wmax, **kwargs):
        minlines = kwargs.pop('minlines', 1)
        wav = kwargs.pop('wavelength', 'air')
        wav += '_wavelength'
        max_J = kwargs.pop('max_J', None) 
        max_v = kwargs.pop('max_v', None) 
        q = 'select * from upper_states where id in'
        q+= '(select upper_state from lines where ' + wav + ' between '
        q+= str(wmin) + ' and ' + str(wmax) + ')'
        if max_v is not None:
            q+= ' and v <= ' + str(max_v)
        if max_J is not None:
            q+= ' and J <= ' + str(max_J)
        upper_states = pandas.read_sql_query(q, self.conn)

        out = []
        for state_id in upper_states.id:
            q2 = 'select ' + wav + ', A from lines where ' + wav
            q2+= ' between ' + str(wmin) + ' and ' + str(wmax)
            q2+= ' and upper_state = ' + str(state_id)
            lines = pandas.read_sql_query(q2, self.conn)
            out.append(lines)
        #return {'specs' : out, 'states' : upper_states}
        return SpecDB._remove_states_with_too_few_lines(minlines,
                out,upper_states)

    @staticmethod
    def _remove_states_with_too_few_lines(minlines, specs, states):
        specs_filtered = []
        rows_to_drop = []
        numlines = []
        for i, spec in enumerate(specs):
            if len(spec) >= minlines:
                specs_filtered.append(spec)
                numlines.append(len(spec))
            else:
                rows_to_drop.append(i)
        states.drop(rows_to_drop, inplace = True)
        states.reset_index(drop=True, inplace = True)
        states['numlines'] = numlines
        return {'specs':specs_filtered, 'states':states}

def puke_spectrum(params, **kwargs):
    step = kwargs.pop('step', params['wav_step'].value)    
    wmin = kwargs.pop('wmin', params['wav_start'].value)
    wmax = kwargs.pop('wmax', None)
    sims = kwargs.pop('sims', {})

    if wmax is None:
        wmax = (params['wav_start'].value + 
                params['wav_step'].value * params.number_of_pixels + 
                params['wav_2nd'].value * params.number_of_pixels**2) 

    spectra = []
    
    for specie in params.info['species']:
        temp_spec = sims[specie].get_spectrum(params[specie+'_Trot'].value,
                                                            params[specie+'_Tvib'].value,
                                                            wmin = wmin, wmax = wmax,
                                                            as_spectrum = False)
        temp_spec[:,1] *= params[specie+'_intensity'].value   
        # print 'intensity: ', params[specie+'_intensity'].value  
        # print 'from DB: '
        # print temp_spec[:,1]
        spectra.append(temp_spec)
    if len(spectra) == 0:
        warnings.warn('No simulation files given, returning empty spectrum!', Warning)
        return spectrum.Spectrum(x = [], y = [])

    spec = numpy.concatenate(spectra)
    spec = spec[spec[:,0].argsort()]
    spec = spectrum.Spectrum(x=spec[:,0], y = spec[:,1])
    spec.refine_mesh()
    #print "after refine: "
    #print max(spec.y)
    spec.convolve_with_slit_function(gauss = params['slitf_gauss'].value, 
                                          lorentz = params['slitf_lorentz'].value, 
                                          step = step)
    #print 'after convolve'
    #print max(spec.y)
    if len(spec.y)>0:
        spec.y += params['baseline'].value
        spec.y += params['baseline_slope'].value*(spec.x-params['wav_start'].value)

    #print spec.y
    return spec
