﻿LICENSE TERMS
effective upon 1st November 2017
These license terms constitute basic conditions for the use of the software defined below as well as the agreement on the use of such software between the end-user defined below and the Provider, which is for the needs of these license terms Masaryk University, ID 00216224, with the registered office at Žerotínovo nám. 617/9, 602 00 Brno, the Czech Republic (hereinafter referred to as „License Terms“):


1. SUBJECT AND PURPOSE OF THE LICENSE TERMS
1. The Software shall mean the computer program within the meaning of Art. 1 of the Directive 2009/24/EC of the European Parliament and of the Council of 23 April 2009 on the legal protection of computer programs (hereinafter referred to as the „Directive“) downloaded from the Provider’s website as the interactive application called massiveOES (available from: https://bitbucket.org/OES_muni/massiveoes) and in the form of executable code, where the Software contains the computer program itself as well as the documentation in electronic form.
2. The Provider is employer of the authors of Software and as such the Provider is executor of the relevant intellectual property rights associated with or related to the Software; the Provider hereby declares that the Software is free of any apparent factual or legal defects and as such it is fully capable of general use.
3. The End-user shall mean natural person or legal entity entitled to use the Software under the applicable laws and these License Terms.
4. Purpose of the License Terms is to set mutual rights and duties of the Provider and the End-user when using the Software within the meaning of applicable laws in order to conclude a license agreement by the End-user with the Provider through these License Terms.


1. LICENSE
1. The Provider hereby grants the End-user the non-exclusive right for the duration of the copyright to use the Software, limited in the way of use to reproduction of the Software within the meaning of the applicable laws necessary for loading, displaying, running, transmission or storage of the computer program to the memory of End-user’s computers, working stations, terminals, phones or other electronic digital devices, where there is a possibility to access, modify, translate or reproduce the Software within the commercial or non-commercial purpose (hereinafter referred to as „License“).
2. The License is provided free of charge.
3. The End-user is not entitled in any way to rent, lend, share or otherwise use the Software, sublicense the Software to other subjects (natural persons or legal entities) or otherwise allow to the third persons its use.
4. The License does not prevent the End-user from observing, studying or testing the functioning of the Software in order to determine the ideas and principles which underlie any element of the Software, or making of a back-up copy necessary for the use of the Software within the meaning of Art. 5 of the Directive.
5. Results of use of the Software are intellectual property of the End-user and the Provider is not entitled to treat or limit the treatment of such intellectual property; the only exception to that is Art. II.6. of these License Terms.
6. In case of modifying, translation or other reproduction of the Software, or in case of using or publishing the results of use of the Software with the commercial or non-commercial purpose, it is always necessary to provide – in accordance with the applicable practices – distinctive and clearly recognizable information of this wording:
„The original software is massiveOES developed by the Masaryk University available from: https://bitbucket.org/OES_muni/massiveoes.“
Likewise, it is always necessary to quote the following titles according to the appropriate and relevant citation guidelines:
„VORÁČ, Jan; SYNEK, Petr; PROCHÁZKA, Vojtěch; HODER, Tomáš. State-by-state emission spectra fitting for non-equilibrium plasmas: OH spectra of surface barrier discharge at argon/water interface. Journal of Physics D: Applied Physics. 2017, 50(29), 294002. DOI: https://doi.org/10.1088/1361-6463/aa7570.“
and
„VORÁČ, Jan; SYNEK, Petr; POTOČŇÁKOVÁ, Lucia; HNILICA, Jaroslav; KUDRLE, Vít. Batch processing of overlapping molecular spectra as a tool for spatio-temporal diagnostics of power modulated microwave plasma jet. Plasma Sources Science and Technology 26.2 (2017), 025010. DOI: https://doi.org/10.
1088/1361-6595/aa51f0.“


1. LIABILITIES
1. The Provider shall not be liable as far as possible according to the applicable laws for direct or indirect damages in any form, including the subsequent damages, incidental damages, loss due to the lost business profits, business disruption, other End-user’s financial loss or loss of data; the Software shall be provided “as is” with no warranties in terms of functionality, production of the results, marketability or suitability for the purpose and intent of the End-user.
2. The Provider shall not be liable for the correctness and completeness of the results of the use of the Software.
3. Any unauthorized End-user’s interventions or the infringement of the Provider’s intellectual property will be prosecuted and sanctioned in accordance with applicable laws.
4. The Provider shall not be liable for any treatment of the third person’s personal data by the End-user, who treats them when using the Software; the End-user is obliged to secure the proper protection of such third person’s personal data prior to the first use, as well as throughout the use of the Software.
 
1. FINAL PROVISIONS
1. These License Terms become effective as of the first use of the Software.
2. The first use of the Software expresses the End-user's acceptance of the License Terms.
3. These License Terms express the complete arrangement between the Provider and the End-user related to the Software and they replace any prior arrangements about the Software.
4. Relations arising out of these License Terms shall be governed by and construed in accordance with the laws of the Czech Republic; any disputes based on the obligations arising out of the License Terms or the License itself will be ruled and settled by the courts of the Czech Republic.
5. If any of the provisions of these License Terms shall become or be held invalid or ineffective, all other provisions hereof shall remain in effect. The parties of these License Terms shall amend and replace invalid or ineffective provision by a valid and effective provision which accomplishes as far as possible the purpose and the intent of the invalid or ineffective provision; the same shall apply in the case of an omission.
6. The End-user hereby claims, he/she approves these License Terms and declares their content is clear and understandable to him/her; his/her will to conclude a license agreement with the Provider through the License Terms is serious and free.