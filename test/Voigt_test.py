# -*- coding: utf-8 -*-
"""
Created on Mon Aug 10 15:41:06 2015

@author: Petr
"""

from pylab import *
from massiveOES.spectrum import Voigt

x = np.linspace(300.0, 400.0, num = 10000 )

result = Voigt(x, alphaD = 1e-20, alphaL = 1e-20, nu_0 = 350.0, A=1)

plot (result, '+')
show()