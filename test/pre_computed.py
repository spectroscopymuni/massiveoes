from massiveOES import SpecDB, SimulatedSpectra

n2DB = SpecDB('../massiveOES/data/N2CB.db')
SS = SimulatedSpectra(species = 'N2CB')

for Trot in (range(300, 600, 80) 
             + range(600, 1000, 150)
             + range(1000,2000,700)
             + range(2000, 8000, 1000)):
    for Tvib in (range(300, 1000, 120) +
                 range(1000, 10000, 1500)    ):
        try:

            spec = n2DB.get_spectrum(Trot, Tvib)
            spec.y = spec.y[spec.x.argsort()]
            spec.x = spec.x[spec.x.argsort()]
            SS.spectra[(Trot, Tvib)] = spec
        except:
            print Trot, Tvib

SS.save('N2CB.pkl')
