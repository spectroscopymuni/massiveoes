##package for batch processing of optical emission spectra
##Authors: Jan Vorac and Petr Synek
##contact: vorac@mail.muni.cz, synek@physics.muni.cz

from setuptools import setup, find_packages
setup(
    name = "massiveOES",
    version = "1.0",
    packages = find_packages(),
)
